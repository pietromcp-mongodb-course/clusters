#!/bin/bash

mongo mongodb://mongo2:30002 lock-write.js

mongodump --db test --host mongo2 --port 30002 -o backup/snapshot
mongodump --db other-test --host mongo2 --port 30002 -o backup/snapshot

mongo mongodb://mongo2:30002/local read-last-oplog-ts.js | grep '^Timestamp' > backup/last-ts

mongo mongodb://mongo2:30002 unlock-write.js
