#!/bin/bash

mongorestore --host mongo100 --port 30100 backup/snapshot

for dir in $(find backup/delta -maxdepth 1 -type d -name '2023*' | sort) ; do
	mongorestore --host mongo100 --port 30100 --oplogReplay $dir
done
