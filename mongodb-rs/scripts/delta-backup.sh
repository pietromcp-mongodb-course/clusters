#!/bin/bash

mongo mongodb://mongo2:30002 lock-write.js

last_ts=$(cat backup/last-ts | tr -d 'a-z' | tr -d 'A-Z' | tr -d ' ()')
first=$(echo $last_ts | cut -d ',' -f 1)
second=$(echo $last_ts | cut -d ',' -f 2)
query="{ \"ts\" : { \"\$gt\" :  { \"\$timestamp\": { \"t\": $first, \"i\": $second } } } }"

echo "QUERY ==> $query"

mongodump --host mongo2 --port 30002 --db local --collection oplog.rs -o backup/delta/$(date +%Y%m%d%H%M%S) --query "$query"

mongo mongodb://mongo2:30002/local read-last-oplog-ts.js | grep '^Timestamp' > backup/last-ts

mongo mongodb://mongo2:30002 unlock-write.js
