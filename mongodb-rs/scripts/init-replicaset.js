rs.initiate(
   {
      _id: "my-rs",
      version: 1,
      members: [
         { _id: 0, host : "mongo0:30000" },
         { _id: 1, host : "mongo1:30001" },
         { _id: 2, host : "mongo2:30002" }
      ]
   }
)
