# ReplicaSet 0
- mongo00:27000
- mongo01:27001
- mongo02:27002

# ReplicaSet 1
- mongo10:27100
- mongo11:27101
- mongo12:27102

# ReplicaSet 2
- mongo20:27200
- mongo21:27201
- mongo22:27202

# ReplicaSet 3
- mongo30:27300
- mongo31:27301
- mongo32:27302

# Router
- router00:28000
- router01:28001