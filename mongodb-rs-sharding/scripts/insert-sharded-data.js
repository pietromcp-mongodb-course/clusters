use bigdb

for(let i = 0; i < 10000; i++) {
	db.bigcollx.insertOne({
		x: (i % 5),
		y: i,
		z: i * i,
		text: "ABCDEFGHIJKLMNOPQRSTUVWXZ" + i,
		other_text0: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et commodo urna, quis tempus est. Proin laoreet libero nec nunc accumsan consectetur. Pellentesque lacinia convallis leo, a consectetur dolor maximus a. Maecenas mattis consectetur leo, a accumsan nulla commodo et. Sed dapibus, sapien et luctus efficitur, massa mauris hendrerit dolor, at fermentum libero lectus eu arcu. In hac habitasse platea dictumst. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum ornare mi bibendum blandit. Aliquam erat volutpat. Fusce dui metus, porttitor id porta ut, scelerisque finibus magna. Aliquam mattis lorem eu congue convallis. Nam sem risus, imperdiet ac tincidunt non, accumsan in tellus.",
		other_text1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et commodo urna, quis tempus est. Proin laoreet libero nec nunc accumsan consectetur. Pellentesque lacinia convallis leo, a consectetur dolor maximus a. Maecenas mattis consectetur leo, a accumsan nulla commodo et. Sed dapibus, sapien et luctus efficitur, massa mauris hendrerit dolor, at fermentum libero lectus eu arcu. In hac habitasse platea dictumst. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum ornare mi bibendum blandit. Aliquam erat volutpat. Fusce dui metus, porttitor id porta ut, scelerisque finibus magna. Aliquam mattis lorem eu congue convallis. Nam sem risus, imperdiet ac tincidunt non, accumsan in tellus.",
		other_text2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam et commodo urna, quis tempus est. Proin laoreet libero nec nunc accumsan consectetur. Pellentesque lacinia convallis leo, a consectetur dolor maximus a. Maecenas mattis consectetur leo, a accumsan nulla commodo et. Sed dapibus, sapien et luctus efficitur, massa mauris hendrerit dolor, at fermentum libero lectus eu arcu. In hac habitasse platea dictumst. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum ornare mi bibendum blandit. Aliquam erat volutpat. Fusce dui metus, porttitor id porta ut, scelerisque finibus magna. Aliquam mattis lorem eu congue convallis. Nam sem risus, imperdiet ac tincidunt non, accumsan in tellus."
	})
	if(i % 500 === 0) {
		print(i)
	}
}